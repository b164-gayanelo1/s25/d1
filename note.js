// Example Scenario:

name:"jane",
ernollment:[

	{
		courseId:"1",
		status:"ongoing"
	},
	{
		courseID:"2", 
		status:"completed"
	}

]

name:"Kelly",
ernollment:[

	{
		courseId:"3",
		status:"completed"
	},
	{
		courseID:"2", 
		status:"drop"
	}

]


// MongoDB Aggregation
	// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.


// Using the aggregate method
	//1st Stage
	// $match -> is used to pass the documents the meet the specificed condition(1st stage).
	/*
		Syntax:
		{$match: {field:value}}
	*/

		db.fruits.aggregate([
		    {
		        $match:{onSale: true}
		    }
		])

	//2nd Stage
	// $group -> is used to group elemts together and field-value pairs using the data from the grouped elements.
	/*
		Syntax:
		{$group: {_id:"value", fieldResult:"value"}}
	*/

		db.fruits.aggregate([
		    {
		        $match:{onSale: true}
		    }, 
		    {
		        $group:{_id:"$supplier_id", totalStocks:{$sum:"$stock"}}
		    }
		])

	// The "$" symbol refer to a field name that is available in the documents that are being aggregated on.
	// The "$sum" operator will total the values of all "stocks" fields in the returned documents that are found using the "$match" criteria.


	// Field Projection with aggregration
		// $project -> can be used when aggregating data to include or exclude fields from the returend results.
		/*
			Syntax:
			{$project: {field:1/0}}
		*/
			db.fruits.aggregate([
			    {
			        $match:{onSale: true}
			    }, 
			    {
			        $group:{_id:"$supplier_id", totalStocks:{$sum:"$stock"}}
			    },
			    {
			        $project:{_id:0}
			    }
			])


	// If we want to count all the documents

		db.fruits.aggregate([
		 
		    {
		        $group:{_id:null, myCount:{$sum:1}}
		    },
		    {
		        $project:{_id:0}
		    }
		])

		//If you specify an _id:value of null, or any other constant value, the $group stage calculate accumulated values for all the input documents as a whole.

	
	// Sorting aggregated results
		// $sort -> it can be used to change the order of the aggregated results
		/*
			Syntax:
				{$sort:{field: -1/1}} -- (-1) will sor the result in a reverse order.
		*/

			db.fruits.aggregate([
			    {
			        $match:{onSale: true}
			    }, 
			    {
			        $group:{_id:"$supplier_id", totalStocks:{$sum:"$stock"}}
			    }, 
			    {
			        $sort:{totalStocks:1}
			    }
			])


	// Aggregrating results based on array fields
		// $unwind -> deconstructs an array field from a collection/field with an array value to output a result for each element.
		/*
			{$unwind:"$origin"}
		*/

			db.fruits.aggregate([
			    { 
			        $unwind: "$origin" 
			    }
			])

			//^ The syntax will return results creating separate documents for each of the countries 


			db.fruits.aggregate([
			    { 
			        $unwind: "$origin"
			    }, 
			    {
			        $group:{_id: "$origin", kinds:{$sum:1}}
			    }
			])


	//$count
	db.scores.insertMany([
		{ "_id" : 1, "subject" : "History", "score" : 88 },
		{ "_id" : 2, "subject" : "History", "score" : 92 },
		{ "_id" : 3, "subject" : "History", "score" : 97 },
		{ "_id" : 4, "subject" : "History", "score" : 71 },
		{ "_id" : 5, "subject" : "History", "score" : 79 },
		{ "_id" : 6, "subject" : "History", "score" : 83 }
	])


	/*

	The following aggregation operation has two stages:

	1. The $match stage excludes documents that have a score value of less than or equal to 80 to pass along the documents with score greater than 80 to the next stage.
	2. The $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field called passing_scores.
	*/


	db.scores.aggregate([
	    { $match: { score: { $gt: 80 } } },
	    { $count: "passing_scores" }
	])


	/*

	{ $count: <string> }
	<string> is the name of the output field which has the count as its value. <string> must be a non-empty string, must not start with $ and must not contain the "." character.

	*/
